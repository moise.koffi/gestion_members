const { z } = require("zod");

const signupSchema = z.object({
    username: z.string({ required_error: "Username est requis" })
        .trim()
        .min(3, { message: "Nom Minimum 3 caractères" })
        .max(255, { message: "Nom Maximum 255 caractères" }),

    email: z.string({ required_error: "Email est requis" })
        .trim()
        .email({ message: "Email invalide" })
        .min(3, { message: "Email Minimum 3 caractères" })
        .max(255, { message: "Email Maximum 255 caractères" }),

    phone: z.string({ required_error: "Phone est requis" })
        .trim()
        .min(3, { message: "Phone Minimum 3 caractères" })
        .max(255, { message: "Phone Maximum 255 caractères" }),

    password: z.string({ required_error: "Password est requis" })
        .min(3, { message: "Password Minimum 3 caractères" })
        .max(255, { message: "Password Maximum 255 caractères" }),
});

module.exports = signupSchema;
