const express = require('express')
const router = express.Router()
const service = require('../controllers/services')

router.get("/service",service)

module.exports = router