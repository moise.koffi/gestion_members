const express = require('express')
const router = express.Router()
const {getAllClients,login,client,createClient,getClientById,deleteClient,updateClient} = require('../controllers/client')   //
const validate = require('../middlewares/validate')
const signupSchemma = require('../validators/client')
const authMiddleware   = require('../middlewares/auth-middleware')

router.get("/",getAllClients)
router.get("/:id",getClientById)
router.post("/",validate(signupSchemma),createClient)
router.delete("/:id",deleteClient)
router.put("/:id",updateClient)
router.post("/login",login)
router.get("/client",authMiddleware ,client)  //
module.exports = router

