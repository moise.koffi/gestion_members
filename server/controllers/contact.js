const mongoose = require('mongoose');
const Contact = require('../models/contact');

const contact = async (req, res) => {
    try {
        // Récupérer les données de la requête
        const requestData = req.body;

        // Créer un nouveau document Contact avec les données reçues
        const data = await Contact.create(requestData);

        // Répondre avec succès et un message approprié
        res.json({ success: true, message: "Message envoyé avec succès", data: data });
    } catch (error) {
        // En cas d'erreur, répondre avec un message d'erreur
        console.error("Erreur lors de l'envoi du message :", error);
        res.status(500).json({ success: false, message: "Échec de l'envoi du message" });
    }
};

module.exports = contact;
