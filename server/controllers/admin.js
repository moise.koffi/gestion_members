const express = require('express')
const Client = require('../models/client');
const Contact = require('../models/contact');
const Service = require('../models/service');

//PARTIE USERS

const getAllUsers = async (req, res) => {
    try {
        const users = await Client.find({}, { password: 0 });
        if (users.length === 0) {
            return res.status(404).json({ success: false, message: "Aucun utilisateur n'est enregistré" });
        }
        res.json({ success: true, data: users });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Erreur d'affichage de la liste utilisateur" });
    }
}

const getUsersById = async(req,res) =>{
    try {
        const id = req.params.id;
        const users = await Client.find({id}, { password: 0 });
        if (users.length === 0) {
            return res.status(404).json({ success: false, message: "Aucun utilisateur n'est enregistré" });
        }
        res.json({ success: true, data: users });
    } catch (error) {
        
    }
}

const deleteUsers = async (req, res) => {
    try {
        const id = req.params.id;
        // Supprimer l'utilisateur
        await Client.findByIdAndDelete(id);
        // Récupérer toutes les données d'utilisateurs après la suppression
        const users = await Client.find({}, { password: 0 });
        res.json({ success: true, message: "Utilisateur supprimé", data: users });
    } catch (error) {
        console.error(error)
        res.status(500).json({ message: "Erreur lors de la suppression de l'utilisateur" });
    }
}

const updateUsers = async(req,res)=>{
    try {
        const id = req.params.id;
        const updatedClient = req.body;
        const updatedData = await Client.findByIdAndUpdate(id, updatedClient, { new: true });
        res.json({ success: true, message: "Utilisateur mis à jour", data: updatedData });
    } catch (error) {
        console.error(error)
        res.status(500).json({ message: "Erreur lors de la modification des données de l'utilisateur" });
    }
}

//PARTIE CONTACT

const getAllContacts = async (req, res) => {
    try {
        const data = await Contact.find({});
        if (data.length === 0) {
            return res.status(404).json({ success: false, message: "Aucun contact n'est enregistré" });
        }
        res.json({ success: true, data: data });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Erreur d'affichage de la liste Message" });
    }
}

const deleteContacts = async(req,res)=>{
    try {
        const id = req.params.id;
        await Contact.findByIdAndDelete(id);
        const contacts = await Contact.find({});
        res.json({ success: true, message: "Contact supprimé", data: contacts });
    } catch (error) {
        console.error(error)
        res.status(500).json({ message: "Erreur lors de la suppression de contact" });
    }
}

//PARTIE SERVICES

const getAllServices = async(req, res)=>{
    try {
        const data = await Service.find({});
        if (data.length === 0) {
            return res.status(404).json({ success: false, message: "Aucun contact n'est enregistré" });
        }
        res.json({ success: true, data: data });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Erreur d'affichage de la liste des services" });
    }
}

const getServicesById = async(req, res)=>{
    try {
        const id = req.params.id;
        const data = await Service.find({id});
        if (data.length === 0) {
            return res.status(404).json({ success: false, message: "Aucun contact n'est enregistré" });
        }
        res.json({ success: true, data: data });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Erreur d'affichage de la liste des services" });
    }
}

const createServices = async(req,res)=>{
    try {
        const response = req.body
        const data = await Service.create(response)
        res.json({ success: true,message:"Service crée avec succès", data: data });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Erreur lors de la creation d'un service" });
    }
}

const deleteServices = async(req,res)=>{
    try {
        const id = req.params.id
         await Service.findByIdAndDelete(id);
        const data = await Service.find({})
        res.json({ success: true,message:"Service supprimé avec succès", data: data });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Erreur lors de la suppression d'un service" });
    }
}

const updateServices = async(req,res)=>{
    try {
        const id = req.params.id
        const response = req.body
        const data = await Service.findByIdAndUpdate(id, response, { new: true });
        res.json({ success: true,message:"Service modifié avec succès", data: data });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Erreur lors de la modification d'un service" });
    }
}


module.exports = {
    getAllUsers,
    getAllContacts,
    deleteUsers,
    getUsersById,
    updateUsers,
    deleteContacts,
    getAllServices,
    getServicesById,
    createServices,
    deleteServices,
    updateServices,
}
