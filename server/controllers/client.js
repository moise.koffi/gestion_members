const mongoose = require('mongoose');
const Client = require('../models/client');
const bcrypt = require('bcryptjs');

// Function pour afficher tout les utilisateur
const getAllClients = async (req, res) => {
  try {
    const data = await Client.find({});
    res.json({ success: true, data });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Erreur d'affichage de la liste utilisateur" });
  }
};

// Function afficher un client selon l'id
const getClientById = async (req, res) => {
  try {
    const id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({ message: "Id client invalide" });
    }

    const data = await Client.findById(id);

    if (!data) {
      return res.status(404).json({ message: "Client no trouvé" });
    }

    res.json({ success: true, data });
  } catch (error) {
    console.error(error);
    res.status(400).json({ message: "Error d'affichage client" }); 
  }
};

// Function pour creer un utilisateur
const createClient = async (req, res) => {
  try {
    const { username, email, phone, password } = req.body;

    // Vérifier si l'email existe déjà
    const existingClient = await Client.findOne({ email });
    if (existingClient) {
      return res.status(400).json({ message: "L'adresse e-mail existe déjà" });
    }
    
    //Hasher le mot de passe
      const saltRound = 10
      const hash_password = await bcrypt.hash(password,saltRound)

    // Créer un nouvel utilisateur
    const newClient = new Client({ username, email, phone, 
      password:hash_password});

    const savedClient = await newClient.save();

    res.json({ success: true, message: "utilisateur ajouté", data: savedClient,
    token: await savedClient.generateToken(), 
    clientId : savedClient._id.toString(), });
  } catch (error) {
    next(error);
    res.status(400).json({ message: "Erreur lors de la création de l'utilisateur" });
    
  }
};


// Function pour effacer un client via l'id
const deleteClient = async (req, res) => {
  try {
    const id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({ message: "ID client inavalide" });
    }

    const deletedClient = await Client.findByIdAndDelete(id);

    if (!deletedClient) {
      return res.status(404).json({ message: "Client non trouvé" });
    }

    res.json({ success: true, message: "utilisateur supprimé" });
  } catch (error) {
    console.error(error);
    res.status(400).json({ message: "Echec de la suppresion" }); 
  }
};

// Function pour modifier un client via son id
const updateClient = async (req, res) => {
  try {
    const id = req.params.id;
    const updatedClient = req.body;

    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(400).json({ message: "ID client inavalide" });
    }

    const validationError = new Client(updatedClient).validateSync(); 

    if (validationError) {
      return res.status(400).json({ message: validationError.message });
    }

    const updatedData = await Client.findByIdAndUpdate(id, updatedClient, { new: true });

    if (!updatedData) {
      return res.status(404).json({ message: "Client non trouvé" });
    }

    res.json({ success: true, message: "Utilisateur mis à jour", data: updatedData });
  } catch (error) {
    console.error(error);
    res.status(400).json({ message: "Ereur de la mise à jour utilisateur" }); 
  }
};

const login = async (req, res) => {
  try {
    const { email, password } = req.body;
    const userExist = await Client.findOne({ email });

    if (!userExist) {
      return res.status(404).json({ message: "Utilisateur non trouvé" });
    }

    const isPasswordValid = await bcrypt.compare(password, userExist.password);

    if (isPasswordValid) {
      res.json({ 
        success: true, 
        message: "Connexion réussie", 
        data: userExist,
        token: await userExist.generateToken(), 
        clientId: userExist._id.toString() 
      });
    } else {
      res.status(401).json({ message: "Email ou mot de passe invalide" });
    }
  } catch (error) {
    res.status(500).json({ message: "Erreur de connexion" });
  }
};

const client = async (req, res) => {
  try {
    const clientData = req.user; 
    console.log(clientData);
    res.status(200).json({ success: true, message: "Bonjour cher utilisateur", data: clientData });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: `Une erreur s'est produite lors de la récupération des données du client${error}` });
  }
};




module.exports ={
    updateClient,
    getAllClients,
    getClientById,
    deleteClient,
    createClient,
    login,
    client
}