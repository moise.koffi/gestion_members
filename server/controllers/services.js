const Service = require('../models/service');

const services = async (req, res) => {
  try {
    const response = await Service.find({});
    if (response.length === 0) {
      return res.status(404).json({ success: false, message: "Aucun service n'est disponible" });
    }
    res.json({ success: true, data: response });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: "Une erreur s'est produite lors de la récupération des services" });
  }
};

module.exports = services;
