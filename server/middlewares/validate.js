const validate = (schema) => async (req, res, next) => {
    try {
      const parsedBody = await schema.parseAsync(req.body);
      req.body = parsedBody;
      next();
    } catch (err) {
      const status = 422;
      const message = "Erreur de validation";
      const extraDetails = err.errors[0].message;
      const error = {
        status,
        message,
        extraDetails
      };
      console.log(error);
      res.status(400).json({ msg: "Échec de validation", error });
    }
  };
  
  module.exports = validate;
  