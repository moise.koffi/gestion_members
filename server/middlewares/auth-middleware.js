const jwt = require('jsonwebtoken');
const Client = require('../models/client');

const authMiddleware = async (req, res, next) => {
    try {
        const token = req.header('Authorization');
        
        if (!token) {
            return res.status(401).json({ message: 'HTTP non Authoriser, Token non reçu' });
        }

        const jwtToken = token.replace('Bearer', '').trim(); // Supprimer "Bearer" et espaces en trop
        console.log('Forme du token identifié :', jwtToken);
        
        let isVerified;
        try {
            isVerified = jwt.verify(jwtToken, process.env.JWT_SECRET_KEY); // Vérifier le token JWT
            console.log('JWT Token vérifié :', isVerified);
        } catch (err) {
            // Gérer les erreurs JWT spécifiques ici
            return res.status(401).json({ message: 'Token JWT invalide' });
        }
        
        const userData = await Client.findOne({ email: isVerified.email }).select({ password: 0 });
        console.log('Données utilisateur :', userData);
        
        if (!userData) {
            return res.status(401).json({ message: 'Utilisateur non trouvé dans la base de données' });
        }
        
        req.user = userData;
        req.token = token;
        req.userID = userData._id;
        
        next();
    } catch (error) {
        console.error(error);
        return res.status(500).json({ message: 'Erreur de serveur' });
    }
};

module.exports = authMiddleware;
