const adminMiddleware = async (req, res, next) => {
    try {
        if (!req.user || !req.user.isAdmin) {
            return res.status(403).json({ message: "Accès refusé. L'utilisateur n'est pas un administrateur." });
        }
        next();
    } catch (error) {
        next(error);
    }
};

module.exports = adminMiddleware;
