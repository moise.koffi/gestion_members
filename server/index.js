require('dotenv').config({path: "./utils/.env"})
const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const Client = require('./models/client')
const clientRouter = require('./routes/client')
const errorMiddleware = require('./middlewares/error')
const contactRouter = require('./routes/contact')
const serviceRouter = require('./routes/service')
const adminRouter = require('./routes/admin')
const app = express()
app.use(express.json())
app.use(cors())
const PORT = process.env.PORT || 3001


app.use("/api/Users",clientRouter)
app.use("/api/Form",contactRouter)
app.use("/api/data",serviceRouter)
app.use("/api/admin",adminRouter)

app.use(errorMiddleware)

mongoose.connect("mongodb://localhost:27017/Mern_Admin")
.then(()=>{
    app.listen(PORT,()=>{console.log("Connexion reussie")})
})
.catch((err)=>{console.log(err)})