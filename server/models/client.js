const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')
// const bcrypt = require('bcryptjs');

const clientSchema = mongoose.Schema({
    username: { type: String, required: true },
    email: { type: String, required: true },
    phone: { type: Number, required: true },
    password: { type: String, required: true },
    isAdmin: { type: Boolean, default: false }, 
    date: { type: Date, default: Date.now }
}, { timestamps: true })


// clientSchema.methods.comparePassword = async function(password){
//     return bcrypt.compare(password, this.password)
// }


clientSchema.methods.generateToken = async function(){
try {
    return jwt.sign({
        clientId: this._id.toString(),
        email: this.email,
        isAdmin: this.isAdmin
    },
    process.env.JWT_SECRET_KEY,{
      expiresIn: "30d"  
    }
    )

} catch (error) {
    console.error(error)
};

}
// clientSchema.pre("save",async function(next){
//     const client = this;

//     if(!client.isModified("password")){
//         next()
//     }

//     try {
//         //Hasher le mot de passe
//       const saltRound = 10
//       const hash_password = await bcrypt.hash(client.password,saltRound)
//       client.password = hash_password
//     } catch (error) {
//         next(error) 
//     }
// })
const Client = mongoose.model("Client",clientSchema)
module.exports = Client