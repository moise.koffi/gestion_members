/* eslint-disable react/prop-types */
import { createContext, useContext, useState, useEffect } from "react";

export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  // Utilisez useEffect pour initialiser le token au montage du composant
  useEffect(() => {
    const tokenFromLS = localStorage.getItem("token");
    setToken(tokenFromLS);
    getServices();
  }, []);


const getServices = async () => {
  try {
    const response = await fetch("http://localhost:3001/api/data/service", {
      method: "GET",
      headers: {
        "content-type": "application/json",
      },
    });
    if (response.ok) {
      const data = await response.json();
      console.log(data.message);
      setServices(data.message);
    }
  } catch (error) {
    console.error("Erreur du services Frontend", error);
  }
};


const [Data, setData] = useState("");
// Utilisez un état pour stocker le token
const [token, setToken] = useState(localStorage.getItem("token"));
const [services, setServices] = useState("");
// Utilisez un état pour déterminer si l'utilisateur est connecté
const isLoggedIn = !!token;

// Fonction pour stocker le token dans le stockage local
const storeTokenInLS = (serverToken) => {
  try {
    localStorage.setItem("token", serverToken);
    setToken(serverToken);
  } catch (error) {
    console.error(
      "Erreur lors de l'écriture du token dans le stockage local :",
      error
    );
  }
};

// Fonction pour déconnecter l'utilisateur en supprimant le token du stockage local
const logoutUser = () => {
  try {
    localStorage.removeItem("token");
    setToken(null);
  } catch (error) {
    console.error(
      "Erreur lors de la suppression du token depuis le stockage local :",
      error
    );
  }
};

return (
  <AuthContext.Provider
    value={{
      isLoggedIn,
      services,
      storeTokenInLS,
      logoutUser,
      Data,
    }}
  >
    {children}
  </AuthContext.Provider>
);
};

// Hook personnalisé pour gérer l'authentification JWT
export const useJWTAuthentication = () => {
const [userData, setUserData] = useState(null);
const [loading, setLoading] = useState(true); // Ajout d'un état de chargement
const [error, setError] = useState(null); // Ajout d'un état d'erreur

useEffect(() => {
  const authenticateUser = async () => {
    try {
      setLoading(true); // Début du chargement
      const token = localStorage.getItem("token");
      if (!token) {
        // Token non trouvé, déconnexion de l'utilisateur
        setUserData(null);
        setLoading(false); // Fin du chargement
        return;
      }

      const response = await fetch("http://localhost:3001/api/Users/client", {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      if (response.ok) {
        const data = await response.json();
        setUserData(data);
        setLoading(false); // Fin du chargement
      } else {
        // Erreur lors de la vérification du token, déconnexion de l'utilisateur
        setUserData(null);
        setError(new Error("Erreur de vérification du token"));
        setLoading(false); // Fin du chargement
      }
    } catch (err) {
      console.error("Erreur lors de l'authentification :", err);
      setUserData(null);
      setError(new Error("Erreur d'authentification"));
      setLoading(false); // Fin du chargement
    }
  };

  authenticateUser();
}, []);

return { userData, loading, error };
};

export const useAuth = () => {
const authContextValue = useContext(AuthContext);
if (!authContextValue) {
  throw new Error("useAuth doit être utilisé dans un AuthProvider");
}
return authContextValue;
};

