import { NavLink } from "react-router-dom";
import "./Navbar.css";
import { useAuth } from "../store/auth";

const Navbar = () => {
  const { isLoggedIn } = useAuth();

  return (
    <header>
      <div className="container">
        <div className="logo-brand">
          <a href="/">KoffiMoise</a>
        </div>
        <nav>
          <ul>
            <li>
              <NavLink to="/" activeClassName="active">Home</NavLink>
            </li>
            <li>
              <NavLink to="/about" activeClassName="active">About</NavLink>
            </li>
            <li>
              <NavLink to="/service" activeClassName="active">Services</NavLink>
            </li>
            <li>
              <NavLink to="/contact" activeClassName="active">Contact</NavLink>
            </li>
            {isLoggedIn ? (
              <li>
                <NavLink to="/logout" activeClassName="active">Logout</NavLink>
              </li>
            ) : (
              <>
                <li>
                  <NavLink to="/login" activeClassName="active">Login</NavLink>
                </li>
                <li>
                  <NavLink to="/register" activeClassName="active">Register</NavLink>
                </li>
              </>
            )}
          </ul>
        </nav>
      </div>
    </header>
  );
};

export default Navbar;
