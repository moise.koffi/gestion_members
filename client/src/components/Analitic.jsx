export default function Analitic() {
    return <>
    <section>
        <div className="section-analitycs">
            <div className="container-grid-four-cols">
                <div className="div1">
                    <h2>50+</h2>
                    <p>Compagnies enregistrés</p>
                </div>
                <div className="div1">
                    <h2>100,00+</h2>
                    <p>Clients satisfaits</p>
                </div>
                <div className="div1">
                    <h2>500+</h2>
                    <p>Développeurs connus</p>
                </div>
                <div className="div1">
                    <h2>24/7</h2>
                    <p>Services Client</p>
                </div>
            </div>
        </div>
    </section>
    </>
}