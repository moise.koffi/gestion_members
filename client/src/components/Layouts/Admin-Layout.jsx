import { NavLink,Outlet } from "react-router-dom"
import { FaHome, FaRegListAlt, FaUsers } from "react-icons/fa";
import { FaMessage } from "react-icons/fa6";

export const AdminLayout =()=>{
    return<>
    <header>
        <div className="container">
            <nav>
                <ul>
                    <li> 
                        <NavLink to="/admin/users" activeClassName="active">
                            <FaUsers /> Users
                        </NavLink>
                    </li>
                    <li>
                    <NavLink to="/admin/contacts" activeClassName="active">
                    <FaMessage /> Contacts
                    </NavLink>
                    </li>
                    <li>
                        <NavLink to="/admin/services" activeClassName="active">
                        <FaRegListAlt />  Service
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/" activeClassName="active">
                        <FaHome /> Home
                        </NavLink>
                    </li>
                </ul>
            </nav>
        </div>
    </header>
    <Outlet/>
    </> 
}

