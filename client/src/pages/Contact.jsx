import { useState } from "react";
import { toast } from "react-toastify";
const Contact = () => {
   
    const [formData, setFormData] = useState({
        username: '',
        email: '',
        message: ''
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await fetch("http://localhost:3001/api/Form/contact", {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    
                },
                body: JSON.stringify(formData)
            });
            if (response.ok) {
                // Afficher un message de succès ou effectuer une action appropriée
                toast.success("Formulaire soumis avec succès !");
                console.log("Formulaire soumis avec succès !");
                setFormData({username:"", email:"", message:""})
            } else {
                // Gérer les erreurs de la requête
                console.error("Erreur lors de la soumission du formulaire :", response.statusText);
            }
        } catch (error) {
            console.error("Erreur lors de la soumission du formulaire :", error);
        }
    };

    return (
        <section className="section-contact">
            <div className="contact-content">
                <h1 className="main-heading">Contactez-Nous</h1>
            </div>
            <div className="container-grid-two-cols">
                <div className="contact-img">
                    <img src="/images/contact.jpg" alt="" width="500" height="auto" />
                </div>
                <section className="section-form">
                    <form onSubmit={handleSubmit}>
                        <div>
                            <label htmlFor="username">Username</label>
                            <input type="text" name="username" required id="username" autoComplete="off" value={formData.username} onChange={handleChange} />
                        </div>
                        <div>
                            <label htmlFor="email">Email</label>
                            <input type="email" name="email" required id="email" autoComplete="off" value={formData.email} onChange={handleChange} />
                        </div>
                        <div>
                            <label htmlFor="message">Message</label>
                            <textarea name="message" id="message" cols="30" rows="6" value={formData.message} onChange={handleChange}></textarea>
                        </div>
                        <div className="bouton">
                            <button type="submit">Envoyer</button>
                        </div>
                    </form>
                </section>
            </div>
        </section>
    );
};

export default Contact;
