import { NavLink } from "react-router-dom";
import Analitic from "../components/Analitic";
import { useJWTAuthentication } from "../store/auth"; // Importez le hook useJWTAuthentication

const About = () => {
    const userData = useJWTAuthentication(); // Utilisez le hook useJWTAuthentication

    return (
        <>
            <main>
                <section>
                    <div className="section-hero">
                        <div className="container-grid-twos-cols">
                            <div className="hero-content">
                                <p>
                                    bienvenue {userData ? `${userData.username} sur le site` : `sur le site`}
                                </p>
                                <h1>Pourquoi nous choisir ?</h1>
                                <p>
                                    Chez Koffimoise, nous sommes bien plus qu'une simple plateforme.
                                    Nous sommes votre partenaire dans la réussite de vos projets,
                                    votre guide dans un monde
                                </p>
                                <br />
                                <p>
                                    numérique en constante évolution et votre allié pour concrétiser vos rêves.
                                </p>
                                <br />
                                <p>
                                    Innovation constante : Nous nous engageons à rester à la pointe de la technologie pour vous offrir les solutions les plus innovantes du marché.
                                    Expérience utilisateur exceptionnelle
                                </p>

                                <div>
                                    <NavLink to="/login" className="btn ">
                                        <button className="btn1">
                                            Connectez vous
                                        </button>
                                    </NavLink>
                                    <NavLink to="/contact" className="btn">
                                        <button className="btn1">
                                            Contactez-nous
                                        </button>
                                    </NavLink>
                                </div>

                            </div>
                        </div>
                        <div className="section-image">
                            <img src="/images/about.jpg" alt=""
                                width="400" height="auto"
                            />
                        </div>
                    </div>
                </section>
            </main>
            <Analitic />
        </>
    );
}

export default About;
