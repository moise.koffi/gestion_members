import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
export const AdminUsers = () => {
    const [users, setUsers] = useState([]);

    // Récupération des utilisateurs au montage du composant
    useEffect(() => {
        const getAllUsers = async () => {
            try {
                const token = localStorage.getItem("token");
                if (!token) {
                    throw new Error("Token JWT non trouvé dans localStorage");
                }
                
                const response = await fetch("http://localhost:3001/api/admin/users", {
                    method: 'GET',
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": `Bearer ${token}`
                    }
                });

                if (!response.ok) {
                    throw new Error("Erreur lors de la récupération des utilisateurs:" + response.statusText);
                }

                const data = await response.json();
                console.log("Données des utilisateurs reçues :", data);
                setUsers(data.data);
            } catch (error) {
                console.error("Erreur lors de la récupération des utilisateurs:", error);

            }
        };
        getAllUsers();
    }, []);

    const deleteUser = async (id) => {
        try {
            const token = localStorage.getItem("token");
            if (!token) {
                throw new Error("Token JWT non trouvé dans localStorage");
            }
            const response = await fetch(`http://localhost:3001/api/admin/users/delete/${id}`,{
                method:"DELETE",
                headers:{
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${token}`
                }
                
            })
            if (!response.ok) {
                throw new Error("Erreur lors de la supression des utilisateurs:" + response.statusText);
            }

            const data = await response.json();
            setUsers(data.data);
        } catch (error) {
            console.error("Erreur lors de la suppression de l'utilisateur :", error.message)
        }
    }

    return (
        <>
        <section>
            <div className="container">
                 <h1>Liste des utilisateurs</h1>
            </div>
            <div className="container-admin-users">
            <table>
                    <thead>
                        <tr>
                            <th>Nom d'utilisateur</th>
                            <th>Email</th>
                            <th>Telephone</th>
                            <th>Date</th>
                            <th>Update</th>
                            <th>Supprimer</th>
                        </tr>
                    </thead>
                    <tbody>
                        {users.map((user, index) => (
                            <tr key={index}>
                                <td>{user.username}</td>
                                <td>{user.email}</td>
                                <td>{user.phone}</td>
                                <td>{new Date(user.date).toLocaleDateString()}</td>
                                <td><Link to={`/admin/users/${user._id}/edit`}>Modifier</Link></td>
                                <td>
                                    <button className="btn1" onClick={()=>deleteUser(user._id)}>
                                        Supprimer
                                    </button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>

            </div>
        </section>
            

                
        </>
    );
    
};
