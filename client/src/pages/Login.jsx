import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../store/auth";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Logins = () => {
  const [formData, setFormData] = useState({
    email: "",
    password: ""
  });

  const {storeTokenInLS}= useAuth()

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };
  const navigate = useNavigate()
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch("http://localhost:3001/api/Users/login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(formData)
      });
      
      if (response.ok) {
        toast.success("Connexion réussie !");
        const res_data = await response.json();
        storeTokenInLS(res_data.token)
        setFormData({email:"", password:""})
        navigate("/")
      } else {
        toast.error("Échec de la connexion");
      }
    } catch (error) {
      console.error("Erreur lors de la requête de connexion :", error);
    }
  };
  
  return (
    <section>
      <main>
        <div className="section-registration">
          <div className="container gid grid-two-cols">
            <div className="registration-image">
              <img
                src="/images/registration.jpg"
                alt="Une fille essaye de s'enregistrer"
                width="500"
                height="500"
              />
            </div>
            <div className="registration-form">
              <h1 className="main-heading mb-3">Se Connecter</h1>
              <form onSubmit={handleSubmit}>
                <div>
                  <label htmlFor="email">Email</label>
                  <input
                    type="email"
                    name="email"
                    placeholder="email"
                    id="email"
                    required
                    autoComplete="off"
                    onChange={handleChange}
                    value={formData.email}
                  />
                </div>

                <div>
                  <label htmlFor="password">Password</label>
                  <input
                    type="password"
                    name="password"
                    placeholder="password"
                    id="password"
                    required
                    autoComplete="off"
                    onChange={handleChange}
                    value={formData.password}
                  />
                </div>
                <br />
                <button type="submit" className="bouton">
                  Envoyer
                </button>
              </form>
            </div>
          </div>
        </div>
      </main>
    </section>
  );
}

export default Logins;
