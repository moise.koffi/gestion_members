import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

export const AdminUpdateServices = () => {
    const [data, setData] = useState({
        price: '',
        desciption: '',
        provider: '',
        service: ''
    });
    const navigate = useNavigate()

    const params = useParams();
    console.log("parametre seule utilisateur:", params);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setData({ ...data, [name]: value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const token = localStorage.getItem("token");
            if (!token) {
                throw new Error("Token JWT non trouvé dans localStorage");
            }
            const response = await fetch(`http://localhost:3001/api/admin/services/update/${params.id}`,{
                method:"PATCH",
                headers:{
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${token}`
                },
                body: JSON.stringify(data)
            });
            if (!response.ok) {
                toast.error("Erreur lors de la modifications des services:" + response.statusText);
            }
            toast.success("Modification du service éffectués:" + response.statusText);
            const responseData = await response.json();
            navigate("/admin/services")
            setData(responseData.data);
        } catch (error) {
            console.error("Erreur lors de la modifications de l'utilisateur:", error);
        }
    }
    

    useEffect(() => {
        const singleUser = async () => {
            try {
                const token = localStorage.getItem("token");
                if (!token) {
                    throw new Error("Token JWT non trouvé dans localStorage");
                }

                const response = await fetch(`http://localhost:3001/api/admin/services/${params.id}`, {
                    method: 'GET',
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": `Bearer ${token}`
                    },
                    body: JSON.stringify(data)
                });

                if (!response.ok) {
                    toast.error("Erreur lors de la récupération des services:" + response.statusText);
                }

                const userData = await response.json();
                console.log("Données des services reçues :", userData);
                setData(userData.data);
            } catch (error) {
                console.error("Erreur lors de la récupération des services:", error);
            }
        };

        singleUser();
    }, [params.id]);

    return (
        <>
            <div className="contacts-section">
                <div className="contact-content-container">
                    <h1 className="main-heading">Modifier un service</h1>
                </div>
                <div className="container grid grid-two-cols">
                    <section className="section-form">
                        <form onSubmit={handleSubmit}>
                            <div>
                                <label htmlFor="username">Nom du Service</label>
                                <input type="text" name="service" id="service" autoComplete="on" onChange={handleChange} value={data.service} required/>
                            </div>
                            <div>
                                <label htmlFor="email">Provenance</label>
                                <input type="text" name="provider" id="provider" autoComplete="on" onChange={handleChange} value={data.provider} required/>
                            </div>
                            <div>
                                <label htmlFor="phone">Description</label>
                                <input type="text" name="description" id="description" autoComplete="on" onChange={handleChange} value={data.description} required/>
                            </div>
                            <div>
                                <label htmlFor="phone">Prix</label>
                                <input type="text" name="price" id="price" autoComplete="on" onChange={handleChange} value={data.price} required/>
                            </div>
                            <div>
                                <button type="submit">Modifier</button>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </>
    );
};
