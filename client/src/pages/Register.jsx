import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../store/auth";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
const Registers = () => {
  const [formData, setFormData] = useState({
    username: "",
    email: "",
    phone: "",
    password: ""
  });

  const {storeTokenInLS}= useAuth()
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };
  const navigate = useNavigate()
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch("http://localhost:3001/api/Users/", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(formData)
      });
      
      if (response.ok) {
        const res_data = await response.json();
        console.log(res_data)
        storeTokenInLS(res_data.token)
        // localStorage.setItem('token',res_data)
        toast("Utilisateur enregistré avec succès !");
        setFormData({username: "",email: "",phone: "",password: ""})
        navigate("/login")
        // Redirection ou autre action après inscription réussie
      } else {
        toast("Échec de l'enregistrement de l'utilisateur");
      }
    } catch (error) {
      console.error("Erreur lors de la requête d'enregistrement :", error);
    }
  };

  return (
    <section>
      <main>
        <div className="section-registration">
          <div className="container gid grid-two-cols">
            <div className="registration-image">
              <img
                src="/images/registration.jpg"
                alt="Une fille essaye de s'enregistrer"
                width="500"
                height="500"
              />
            </div>
            <div className="registration-form">
              <h1 className="main-heading mb-3">S'enregistrer</h1>
              <form onSubmit={handleSubmit}>
                <div>
                  <label htmlFor="username">Username</label>
                  <input
                    type="text"
                    name="username"
                    placeholder="username"
                    id="username"
                    required
                    autoComplete="off"
                    onChange={handleChange}
                    value={formData.username}
                  />
                </div>
                <div>
                  <label htmlFor="email">Email</label>
                  <input
                    type="email"
                    name="email"
                    placeholder="email"
                    id="email"
                    required
                    autoComplete="off"
                    onChange={handleChange}
                    value={formData.email}
                  />
                </div>
                <div>
                  <label htmlFor="phone">Télephone</label>
                  <input
                    type="phone"
                    name="phone"
                    placeholder="phone"
                    id="phone"
                    required
                    autoComplete="off"
                    onChange={handleChange}
                    value={formData.phone}
                  />
                </div>
                <div>
                  <label htmlFor="password">Mot de Passe</label>
                  <input
                    type="password"
                    name="password"
                    placeholder="password"
                    id="password"
                    required
                    autoComplete="off"
                    onChange={handleChange}
                    value={formData.password}
                  />
                </div>
                <br />
                <button type="submit" className="bouton">
                  Envoyer
                </button>
              </form>
            </div>
          </div>
        </div>
      </main>
    </section>
  );
};

export default Registers;
