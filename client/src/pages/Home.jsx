import { NavLink } from "react-router-dom"
import Analitic from "../components/Analitic"


const Home =()=>{
    return<>
    <main>
        <section>
        <div className="section-hero">
        <div className="container-grid-twos-cols">
            <div className="hero-content">
                <h1>Bienvenue chez Koffi Moise</h1>
                <p>
                     Nous sommes une équipe passionnée 
                     qui façonne l'avenir numérique avec innovation et expertise. 
                     Chez Koffi Moise, nous croyons en la puissance des solutions technologiques 
                     pour transformer les entreprises et simplifier les processus. 
                     Que vous cherchiez à développer une application sur mesure, 
                     à améliorer votre infrastructure informatique ou à obtenir des conseils stratégiques en technologie, 
                     nous sommes là pour vous accompagner à chaque étape.
                </p>
            <div>
            <NavLink to="/login" className="btn ">
        <button className="btn1">
            Connectez vous
        </button>
    </NavLink>
    <NavLink to="/contact" className="btn">
        <button className="btn1">
            Contactez-nous
        </button>
    </NavLink>
</div>

            </div>
        </div>

        <div className="section-image">
        <img src="/images/home.jpg" alt="" 
        width="500" height="auto"
        />
    </div>
    </div>
        </section>
    </main>
   
   

    <Analitic/>
    <section>
    <div className="section-hero">
        <div className="container-grid-two-cols">
             <div className="section-image">
                <img src="/images/design.jpg" alt="" width=""/>
             </div>
        </div>
        <div className="hero-content">
                <h1>Get Started</h1>
                <p>
                Bienvenue sur notre plateforme ! Que vous soyez un entrepreneur ambitieux, un développeur expérimenté ou simplement curieux d'explorer de nouvelles opportunités, 
                . Rejoignez-nous dès maintenant et commencez à explorer les possibilités illimitées qui s'offrent à vous !
                </p>
            <div>
            <NavLink to="/login" className="btn ">
        <button className="btn1">
            Connectez vous
        </button>
    </NavLink>
    <NavLink to="/contact" className="btn">
        <button className="btn1">
            Contactez-nous
        </button>
    </NavLink>
</div>

            </div>
    </div>
    </section>
   
    
    </>
}
export default Home