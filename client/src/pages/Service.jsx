/* eslint-disable react/prop-types */
import { useEffect, useState } from "react";

const Service = () => {
  const [services, setServices] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch("http://localhost:3001/api/data/service");
        if (!response.ok) {
          throw new Error("Erreur lors de la récupération des services");
        }
        const data = await response.json();
        setServices(data.data);
        setLoading(false);
      } catch (error) {
        console.error(error);
        setError(error.message);
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  if (loading) {
    return <div className="loading">Loading...</div>;
  }

  if (error) {
    return <div className="error-message">{error}</div>;
  }

  return (
    <section className="section-services">
      <div className="container">
        <h1 className="main-heading">Services</h1>
      </div>

      <div className="container-grid-three-cols">
        {services.map((curService, index) => (
          <ServiceCard key={index} service={curService} />
        ))}
      </div>
    </section>
  );
};

const ServiceCard = ({ service }) => {
  return (
    <div className="card">
      <div className="card-img">
        <img src="/images/contact.jpg" alt="" width="200" />
      </div>
      <div className="card-details">
        <div className="grid-grid-two-cols">
          <p>{service.provider}</p>
          <p className="card-prix">{service.price}</p>
        </div>
        <h2>{service.service}</h2>
        <p>{service.description}</p>
      </div>
    </div>
  );
};

export default Service;
