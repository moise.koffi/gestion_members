import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../store/auth";
export const Logout = () => {
  const { logoutUser } = useAuth();
  const navigate = useNavigate();
  const [isLoggedOut, setIsLoggedOut] = useState(false);

  useEffect(() => {
    const handleLogout = async () => {
      logoutUser();
      setIsLoggedOut(true); // Mettre à jour l'état pour indiquer que l'utilisateur s'est déconnecté
      setTimeout(() => navigate("/login"), 2000); // Rediriger après 2 secondes
    };

    handleLogout();
  }, [logoutUser, navigate]);

  // Le composant ne rend rien car il ne nécessite pas d'affichage
  return (
    <div style={{ textAlign: "center", marginTop: "20px" }}>
      {isLoggedOut && (
        <div style={{ color: "green", fontWeight: "bold" }}>
          Déconnexion réussie. Redirection vers la page de connexion...
        </div>
      )}
    </div>
  );
};
