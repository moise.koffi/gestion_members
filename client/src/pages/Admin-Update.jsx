import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

export const AdminUpdate = () => {
    const [data, setData] = useState({
        username: '',
        email: '',
        phone: ''
    });
    const navigate = useNavigate()

    const params = useParams();
    console.log("parametre seule utilisateur:", params);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setData({ ...data, [name]: value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const token = localStorage.getItem("token");
            if (!token) {
                throw new Error("Token JWT non trouvé dans localStorage");
            }
            const response = await fetch(`http://localhost:3001/api/admin/users/update/${params.id}`,{
                method:"PATCH",
                headers:{
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${token}`
                },
                body: JSON.stringify(data)
            });
            if (!response.ok) {
                toast.error("Erreur lors de la modifications de l'utilisateur:" + response.statusText);
            }
            toast.success("Modification éffectués de l'utilisateur:" + response.statusText);
            const responseData = await response.json();
            navigate("/admin/users")
            setData(responseData.data);
        } catch (error) {
            console.error("Erreur lors de la modifications de l'utilisateur:", error);
        }
    }
    

    useEffect(() => {
        const singleUser = async () => {
            try {
                const token = localStorage.getItem("token");
                if (!token) {
                    throw new Error("Token JWT non trouvé dans localStorage");
                }

                const response = await fetch(`http://localhost:3001/api/admin/users/${params.id}`, {
                    method: 'GET',
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": `Bearer ${token}`
                    },
                    body: JSON.stringify(data)
                });

                if (!response.ok) {
                    toast.error("Erreur lors de la récupération de l'utilisateur:" + response.statusText);
                }

                const userData = await response.json();
                console.log("Données de l'utilisateur reçues :", userData);
                setData(userData.data);
            } catch (error) {
                console.error("Erreur lors de la récupération de l'utilisateur:", error);
            }
        };

        singleUser();
    }, [params.id]); // Utilisez params.id pour déclencher l'effet lorsque l'ID des paramètres d'URL change

    return (
        <>
            <div className="contacts-section">
                <div className="contact-content-container">
                    <h1 className="main-heading">Modifier un utilisateur</h1>
                </div>
                <div className="container grid grid-two-cols">
                    <section className="section-form">
                        <form onSubmit={handleSubmit}>
                            <div>
                                <label htmlFor="username">Username</label>
                                <input type="text" name="username" id="username" autoComplete="on" onChange={handleChange} value={data.username} required/>
                            </div>
                            <div>
                                <label htmlFor="email">Email</label>
                                <input type="email" name="email" id="email" autoComplete="on" onChange={handleChange} value={data.email} required/>
                            </div>
                            <div>
                                <label htmlFor="phone">Telephone</label>
                                <input type="number" name="phone" id="phone" autoComplete="on" onChange={handleChange} value={data.phone} required/>
                            </div>
                            <div>
                                <button type="submit">Modifier</button>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </>
    );
};
