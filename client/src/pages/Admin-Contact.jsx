import { useState, useEffect } from "react";
import { toast } from "react-toastify";

export const AdminContacts = () => {
    const [formData, setFormData] = useState([]);

    useEffect(() => {
        const getAllContacts = async () => {
            try {
                const token = localStorage.getItem("token");
                if (!token) {
                    throw new Error("Token JWT non trouvé dans localStorage");
                }
                
                const response = await fetch("http://localhost:3001/api/admin/contacts", {
                    method: 'GET',
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": `Bearer ${token}`
                    }
                });

                if (!response.ok) {
                    throw new Error("Erreur lors de la récupération des contacts:" + response.statusText);
                }
                const data = await response.json();
                console.log("Données des contacts reçues :", data);
                setFormData(data.data);
            } catch (error) {
                console.error("Erreur lors de la récupération des contacts:", error);
            }
        };
        getAllContacts();
    }, []);

    const deleteContacts = async (id) => {
        try {
            const token = localStorage.getItem("token");
            if (!token) {
                throw new Error("Token JWT non trouvé dans localStorage");
            }
            const response = await fetch(`http://localhost:3001/api/admin/contacts/delete/${id}`,{
                method:"DELETE",
                headers:{
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${token}`
                }
                
            })
            if (!response.ok) {
                throw new Error("Erreur lors de la supression des contacts:" + response.statusText);
            }
            toast.success("Contact Supprimé !")
            const data = await response.json();
            setFormData(data.data);
        } catch (error) {
            console.error("Erreur lors de la suppression de contacts :", error.message)
        }
    }



    return (
        <>
        <section>
            <div className="container">
                 <h1>Liste des Contacts</h1>
            </div>
            <div className="container-admin-users">
                <table>
                    <thead>
                        <tr>
                            <th>Nom d'utilisateur</th>
                            <th>Email</th>
                            <th>Message</th>
                            <th>Date</th>
                            <th>Heure</th>
                            <th>Supprimer</th>
                        </tr>
                    </thead>
                    <tbody>
                        {formData.map((contact, index) => (
                            <tr className="custom-row-style"key={index}>
                                <td>{contact.username}</td>
                                <td>{contact.email}</td>
                                <td>{contact.message}</td>
                                <td>{new Date(contact.date).toLocaleDateString()}</td>
                                <td>{new Date(contact.date).toLocaleTimeString()}</td>
                                <td>
                                <button className="btn1" onClick={()=>deleteContacts(contact._id)}>
                                        Supprimer
                                    </button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </section>
        </>
    );
}
