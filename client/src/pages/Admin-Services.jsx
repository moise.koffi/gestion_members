import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
export const AdminServices = () => {
    const [services, setServices] = useState([]);

    // Récupération des utilisateurs au montage du composant
    useEffect(() => {
        const getAllServices = async () => {
            try {
                const token = localStorage.getItem("token");
                if (!token) {
                    throw new Error("Token JWT non trouvé dans localStorage");
                }
                
                const response = await fetch("http://localhost:3001/api/admin/services", {
                    method: 'GET',
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": `Bearer ${token}`
                    }
                });

                if (!response.ok) {
                    throw new Error("Erreur lors de la récupération des services:" + response.statusText);
                }

                const data = await response.json();
                console.log("Données des services reçues :", data);
                setServices(data.data);
            } catch (error) {
                console.error("Erreur lors de la récupération des services:", error);

            }
        };
        getAllServices();
    }, []);

    const deleteUser = async (id) => {
        try {
            const token = localStorage.getItem("token");
            if (!token) {
                throw new Error("Token JWT non trouvé dans localStorage");
            }
            const response = await fetch(`http://localhost:3001/api/admin/services/delete/${id}`,{
                method:"DELETE",
                headers:{
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${token}`
                }
                
            })
            if (!response.ok) {
                throw new Error("Erreur lors de la supression des services:" + response.statusText);
            }
            toast.success("Service supprimé")
            const data = await response.json();
            setServices(data.data);
        } catch (error) {
            console.error("Erreur lors de la suppression de service :", error.message)
        }
    }

    return (
        <>
        <section>
            <div className="container">
                 <h1>Liste des Services</h1>
            </div>
            <div className="container-admin-users">
            <table>
                    <thead>
                        <tr>
                            <th>Nom du Service</th>
                            <th>Provenace</th>
                            <th>Description</th>
                            <th>Prix</th>
                            <th>Modifier</th>
                            <th>Supprimer</th>
                            <th>Ajouter</th>
                        </tr>
                    </thead>
                    <tbody>
                        {services.map((service, index) => (
                            <tr className="custom-row-style1"key={index}>
                                <td>{service.service}</td>
                                <td>{service.provider}</td>
                                <td>{service.description}</td>
                                <td>{service.price}</td>
                                <td><Link to={`/admin/services/${service._id}/edit`}>Modifier</Link></td>
                                <td>
                                    <button className="btn1" onClick={()=>deleteUser(service._id)}>
                                        Supprimer
                                    </button>
                                </td>
                                
                            </tr>
                        ))}
                    </tbody>
                </table>

            </div>
        </section>
            

                
        </>
    );
    
};
