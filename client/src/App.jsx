import {BrowserRouter,Routes,Route} from "react-router-dom"
import Home from "./pages/Home";
import Contact from "./pages/Contact";
import About from "./pages/About";
import Service from "./pages/Service";
import Logins from "./pages/Login";
import Registers from "./pages/Register"
import Navbar from "./components/Navbar";
import { Footer } from "./components/footer/Footer";
import { Logout } from "./pages/Logout";
import { AdminLayout } from "./components/Layouts/Admin-Layout";
import { AdminUsers } from "./pages/Admin-Users";
import { AdminContacts } from "./pages/Admin-Contact";
import { AdminUpdate } from "./pages/Admin-Update";
import { AdminServices } from "./pages/Admin-Services";
import { AdminUpdateServices } from "./pages/Admin-Update-Services";

const App =()=>{
return <>
<BrowserRouter>
<Navbar/>
<Routes>
  <Route path="/" element={<Home/>} />
  <Route path="/about" element={<About/>} />
  <Route path="/contact" element={<Contact/>} />
  <Route path="/service" element={<Service/>} />
  <Route path="/login" element={<Logins/>} />
  <Route path="/register" element={<Registers/>} />
  <Route path="/logout" element={<Logout/>} />

  <Route path="/admin/" element={<AdminLayout/>}>
    <Route path="users" element={<AdminUsers />} />
    <Route path="contacts" element={<AdminContacts />} />
    <Route path="/admin/users/:id/edit" element={<AdminUpdate />} />
    <Route path="services" element={<AdminServices />} />
    <Route path="/admin/services/:id/edit" element={<AdminUpdateServices />} />
  </Route>


</Routes>
<Footer/>
</BrowserRouter>
</>
}

export default App;